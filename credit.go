package main

import (
  "fmt"
  "math"
)

func main() {
  var credit, numYear, rate float64
  var periodicity float64 = 12
  
  fmt.Print("Kredit miqdorini kiriting : ")
  fmt.Scan(&credit)

  fmt.Print("To'lov davri : ")
  fmt.Scan(&numYear)
  
  fmt.Print("Kredit foizini kiriting : ")
  fmt.Scan(&rate)
  rate = rate * 0.01
  
  payment(credit, periodicity, numYear, rate)
}

func payment(credit, periodicity, numYear, rate float64) {
  var pay float64
  var totalPayment float64 = 0.0
  var IA, PA, Bal float64 = 0.0, 0.0, 0.0
  
  pay = (1 - (math.Pow(1+rate/periodicity, -periodicity*numYear))) / (rate / periodicity)
  totalPayment = credit / pay
  Bal = credit
  month := 1

  fmt.Printf("\nM : Tolov : Foiz Miqdori : Asosiy to'lov miqdori : Qarz qoldig'i\n")

  for month <= int(numYear)*int(periodicity) {
    IA = Bal * rate / periodicity
    PA = totalPayment - IA
    Bal = Bal - PA
    fmt.Printf("%v: %f, %f, %f, %f\n", month, totalPayment, PA, IA, Bal)
    month++
  }
}